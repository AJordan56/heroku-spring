# Use Open JDK as base image
FROM openjdk:8-jdk-alpine

# Set the working directory
WORKDIR /

# Take the jar from the build folder and add it to project as springweb.jar
ADD build/libs/springweb-0.0.1-SNAPSHOT.jar springweb.jar

# Expose PORT 8080
EXPOSE 8080

# Invoke java executable and run the springweb.jar file
CMD java -jar springweb.jar